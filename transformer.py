import sys
import pandas as pd
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import XSD
import re

arguments = sys.argv
df = pd.read_csv(arguments[1])

SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")

g = Graph()

g.bind("saref", SAREF)
g.bind("ex", EX)

def create_uri(entity_type, entity_id):
    return EX[f"{entity_type}/{entity_id}"]

def extract_building_id(column_name):
    match = re.match(
        r'DE_KN_(industrial|public|residential)(\d+)',
        column_name
    )
    if match:
        return match.group(1) + match.group(2)
    return None

for index, row in df.iterrows():
    utc_timestamp = row['utc_timestamp']
    timestamp_literal = Literal(utc_timestamp, datatype=XSD.dateTime)

    for column in df.columns[2:]:
        if pd.notna(row[column]):
            building_id = extract_building_id(column)
            device_id = column
            measurement_value = row[column]

            if building_id:
                building_uri = create_uri("building", building_id)
                device_uri = create_uri("device", device_id)
                measurement_uri = create_uri("measurement", f"{index}_{device_id}")
                measurement_literal = Literal(measurement_value, datatype=XSD.float)

                g.add((building_uri, RDF.type, SAREF.Building))
                g.add((device_uri, RDF.type, SAREF.Device))
                g.add((device_uri, SAREF.isLocatedIn, building_uri))
                g.add((measurement_uri, RDF.type, SAREF.Measurement))
                g.add((measurement_uri, SAREF.hasTimestamp, timestamp_literal))
                g.add((measurement_uri, SAREF.hasValue, measurement_literal))
                g.add((device_uri, SAREF.hasMeasurement, measurement_uri))
                g.add((measurement_uri, SAREF.isMeasuredIn, Literal("unit", datatype=XSD.string)))

g.serialize(destination='graph.ttl', format='turtle')
